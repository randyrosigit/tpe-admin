import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {NgReduxModule} from '@angular-redux/store';
import {NgRedux, DevToolsExtension} from '@angular-redux/store';
import {rootReducer, ArchitectUIState} from './ThemeOptions/store';
import {ConfigActions} from './ThemeOptions/store/config.actions';
import {LoadingBarRouterModule} from '@ngx-loading-bar/router';

import {CommonModule} from '@angular/common';
import {HttpClientModule} from '@angular/common/http';
import {AppComponent} from './app.component';

import { Routes, RouterModule } from '@angular/router';
import { AuthGuard } from './_guards';

import {BaseLayoutComponent} from './Layout/base-layout/base-layout.component';
import {PagesLayoutComponent} from './Layout/pages-layout/pages-layout.component';

import { HomeComponent } from './Pages/Home/home.component';
import { SettingsComponent } from './Pages/Home/settings.component';
import { LoginComponent } from './Pages/Login/login.component';

// CATEGORY
import { CategoryList } from './Pages/Category/category.list';
import { CategoryAdd } from './Pages/Category/category.add';
import { CategoryEdit } from './Pages/Category/category.edit';
import { CategoryView } from './Pages/Category/category.view';

// MERCHANT
import { MerchantList } from './Pages/Merchant/merchant.list';
import { MerchantAdd } from './Pages/Merchant/merchant.add';
import { MerchantEdit } from './Pages/Merchant/merchant.edit';
import { MerchantView } from './Pages/Merchant/merchant.view';

// Quests
import { QuestList } from './Pages/Quest/quest.list';
import { QuestAdd } from './Pages/Quest/quest.add';
import { QuestEdit } from './Pages/Quest/quest.edit';
import { QuestView } from './Pages/Quest/quest.view';
import { QuestcompleteRequests } from './Pages/Quest/questcomplete.requests';

// VOUCHER
import { VoucherList } from './Pages/Voucher/voucher.list';
import { VoucherAdd } from './Pages/Voucher/voucher.add';
import { VoucherEdit } from './Pages/Voucher/voucher.edit';
import { VoucherView } from './Pages/Voucher/voucher.view';
import { VoucherredeemRequests } from './Pages/Voucher/voucherredeem.requests';

// POLL
import { PollList } from './Pages/Poll/poll.list';
import { PollAdd } from './Pages/Poll/poll.add';
import { PollEdit } from './Pages/Poll/poll.edit';
import { PollView } from './Pages/Poll/poll.view';

// CUSTOMER
import { CustomerList } from './Pages/Customer/customer.list';
import { CustomerAdd } from './Pages/Customer/customer.add';
import { CustomerEdit } from './Pages/Customer/customer.edit';
import { CustomerView } from './Pages/Customer/customer.view';
import { CustomerInfo } from './Pages/Customer/customer.info';

// USER
import { UserList } from './Pages/User/user.list';
import { UserAdd } from './Pages/User/user.add';
import { UserEdit } from './Pages/User/user.edit';
import { UserView } from './Pages/User/user.view';
import { UserProfile } from './Pages/User/user.profile';

// PAGE
import { PageList } from './Pages/Page/page.list';
import { PageAdd } from './Pages/Page/page.add';
import { PageEdit } from './Pages/Page/page.edit';
import { PageView } from './Pages/Page/page.view';

// TEMPLATE
import { TemplateList } from './Pages/Template/template.list';
import { TemplateAdd } from './Pages/Template/template.add';
import { TemplateEdit } from './Pages/Template/template.edit';

// ROLE
import { RoleList } from './Pages/Role/role.list';
import { RoleAdd } from './Pages/Role/role.add';
import { RoleEdit } from './Pages/Role/role.edit';

//ORDERS
import { OrderList } from './Pages/Order/order.list';
import { OrderView } from './Pages/Order/order.view';

import { FAQComponent } from './Pages/Faq/faq.component';

const appRoutes: Routes = [
	{
	    path: '',
	    component: BaseLayoutComponent,
	    canActivate: [AuthGuard],
	    children: [
	      {path: '', component: HomeComponent, data: {extraParameter: 'dashboardsMenu'}},
	      {path: 'settings', component: SettingsComponent,  data: {extraParameter: 'dashboardsMenu'}},
	      {path: 'category', component: CategoryList, data: {extraParameter: 'dashboardsMenu', role: 'category'}},
	      {path: 'category/add', component: CategoryAdd, data: {extraParameter: 'dashboardsMenu'}},
	      {path: 'category/edit/:id', component: CategoryEdit, data: {extraParameter: 'dashboardsMenu'}},
	      {path: 'category/view/:id', component: CategoryView, data: {extraParameter: 'dashboardsMenu'}},
	      {path: 'merchant', component: MerchantList, data: {extraParameter: 'dashboardsMenu'}},
	      {path: 'merchant/add', component: MerchantAdd, data: {extraParameter: 'dashboardsMenu'}},
	      {path: 'merchant/edit/:id', component: MerchantEdit, data: {extraParameter: 'dashboardsMenu'}},
	      {path: 'merchant/view/:id', component: MerchantView, data: {extraParameter: 'dashboardsMenu'}},
	      {path: 'quest', component: QuestList, data: {extraParameter: 'dashboardsMenu'}},
	      {path: 'quest/add', component: QuestAdd, data: {extraParameter: 'dashboardsMenu'}},
	      {path: 'quest/edit/:id', component: QuestEdit, data: {extraParameter: 'dashboardsMenu'}},
	      {path: 'quest/view/:id', component: QuestView, data: {extraParameter: 'dashboardsMenu'}},
	      {path: 'complete_requests', component: QuestcompleteRequests, data: {extraParameter: 'dashboardsMenu'}},
	      {path: 'voucher', component: VoucherList, data: {extraParameter: 'dashboardsMenu'}},
	      {path: 'voucher/add', component: VoucherAdd, data: {extraParameter: 'dashboardsMenu'}},
	      {path: 'voucher/edit/:id', component: VoucherEdit, data: {extraParameter: 'dashboardsMenu'}},
	      {path: 'voucher/view/:id', component: VoucherView, data: {extraParameter: 'dashboardsMenu'}},
	      {path: 'redeem_requests', component: VoucherredeemRequests, data: {extraParameter: 'dashboardsMenu'}},  
	      {path: 'poll', component: PollList, data: {extraParameter: 'dashboardsMenu'}},
	      {path: 'poll/add', component: PollAdd, data: {extraParameter: 'dashboardsMenu'}},
	      {path: 'poll/edit/:id', component: PollEdit, data: {extraParameter: 'dashboardsMenu'}},
	      {path: 'poll/view/:id', component: PollView, data: {extraParameter: 'dashboardsMenu'}},
	      {path: 'orders', component: OrderList, data: {extraParameter: 'dashboardsMenu'}},
	      {path: 'order/view/:id', component: OrderView, data: {extraParameter: 'dashboardsMenu'}},
	      {path: 'customer', component: CustomerList, data: {extraParameter: 'dashboardsMenu'}},
	      {path: 'customer/add', component: CustomerAdd, data: {extraParameter: 'dashboardsMenu'}},
	      {path: 'customer/edit/:id', component: CustomerEdit, data: {extraParameter: 'dashboardsMenu'}},
	      {path: 'customer/view/:id', component: CustomerView, data: {extraParameter: 'dashboardsMenu'}},
	      {path: 'customer/info/:id', component: CustomerInfo, data: {extraParameter: 'dashboardsMenu'}},
	      {path: 'user', component: UserList, data: {extraParameter: 'dashboardsMenu'}},
	      {path: 'user/add', component: UserAdd, data: {extraParameter: 'dashboardsMenu'}},
	      {path: 'user/edit/:id', component: UserEdit, data: {extraParameter: 'dashboardsMenu'}},
	      {path: 'user/view/:id', component: UserView, data: {extraParameter: 'dashboardsMenu'}},
	      {path: 'page', component: PageList, data: {extraParameter: 'dashboardsMenu'}},
	      {path: 'page/add', component: PageAdd, data: {extraParameter: 'dashboardsMenu'}},
	      {path: 'page/edit/:id', component: PageEdit, data: {extraParameter: 'dashboardsMenu'}},
	      {path: 'page/view/:id', component: PageView, data: {extraParameter: 'dashboardsMenu'}},
	      {path: 'template', component: TemplateList, data: {extraParameter: 'dashboardsMenu'}},
	      {path: 'template/add', component: TemplateAdd, data: {extraParameter: 'dashboardsMenu'}},
	      {path: 'template/edit/:id', component: TemplateEdit, data: {extraParameter: 'dashboardsMenu'}},
	      {path: 'role', component: RoleList, data: {extraParameter: 'dashboardsMenu'}},
	      {path: 'role/add', component: RoleAdd, data: {extraParameter: 'dashboardsMenu'}},
	      {path: 'role/edit/:id', component: RoleEdit, data: {extraParameter: 'dashboardsMenu'}},
	      {path: 'profile', component: UserProfile, data: {extraParameter: 'dashboardsMenu'}},
	      {path: 'faqs', component: FAQComponent, data: {extraParameter: 'dashboardsMenu'}}
	    ]
	},
	{
    path: '',
    component: PagesLayoutComponent,
    children: [
      {path: 'login', component: LoginComponent, data: {extraParameter: ''}}
    ]
  },
]



export const routing = RouterModule.forRoot(appRoutes);