import {Component, HostListener, OnInit} from '@angular/core';
import {ThemeOptions} from '../../../theme-options';
import {select} from '@angular-redux/store';
import {Observable} from 'rxjs';
import {ActivatedRoute} from '@angular/router';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
})
export class SidebarComponent implements OnInit {
  public extraParameter: any;

  constructor(public globals: ThemeOptions, private activatedRoute: ActivatedRoute) {}

  @select('config') public config$: Observable<any>;

  private newInnerWidth: number;
  private innerWidth: number;
  activeId = 'dashboardsMenu';
  disactive: any = {'users': false, 'role': false, 'category': false, 'merchant': false, 'voucher': false, 'redeem_requests': false, 'quest': false, 'complete_requests': false, 'poll': false, 'orders': false, 'customer': false, 'pages': false, 'settings': false, 'faqs': false, 'template': false};

  toggleSidebar() {
    this.globals.toggleSidebar = !this.globals.toggleSidebar;
  }

  sidebarHover() {
    this.globals.sidebarHover = !this.globals.sidebarHover;
  }

  ngOnInit() {
    setTimeout(() => {
      this.innerWidth = window.innerWidth;
      if (this.innerWidth < 1200) {
        this.globals.toggleSidebar = true;
      }
    });

    this.extraParameter = this.activatedRoute.snapshot.firstChild.data.extraParameter;
    const currentUser = JSON.parse(localStorage.getItem('currentUser'));
    if(currentUser) {
      let permissionss = String(currentUser.permissions);

      if(permissionss == 'all') {
        this.disactive = {'user': true, 'role': true, 'category': true, 'merchant': true, 'voucher': true, 'redeem_requests': true, 'quest': true, 'complete_requests': true, 'poll': true, 'orders': true, 'customer': true, 'page': true, 'settings': true, 'faqs': true, 'template': true};
      } else {
        for(let permission of permissionss.split(',')){
          if(permission == '/user/') {
            this.disactive.user = true;
          } else if(permission == '/role/') {
            this.disactive.role = true;
          } else if(permission == '/category/') {
            this.disactive.category = true;
          } else if(permission == '/merchant/') {
            this.disactive.merchant = true;
          } else if(permission == '/voucher/') {
            this.disactive.voucher = true;
          } else if(permission == '/redeem_requests/') {
            this.disactive.redeem_requests = true;
          } else if(permission == '/quest/') {
            this.disactive.quest = true;
          } else if(permission == '/complete_requests/') {
            this.disactive.complete_requests = true;
          } else if(permission == '/poll/') {
            this.disactive.poll = true;
          } else if(permission == '/orders/') {
            this.disactive.orders = true;
          } else if(permission == '/customer/') {
            this.disactive.customer = true;
          } else if(permission == '/page/') {
            this.disactive.page = true;
          } else if(permission == '/settings/') {
            this.disactive.settings = true;
          } else if(permission == '/faqs/') {
            this.disactive.faqs = true;
          } else if(permission == '/template/') {
            this.disactive.template = true;
          }
        }
      }
      
    }
    
    // console.log(permissions);
  }

  @HostListener('window:resize', ['$event'])
  onResize(event) {
    this.newInnerWidth = event.target.innerWidth;

    if (this.newInnerWidth < 1200) {
      this.globals.toggleSidebar = true;
    } else {
      this.globals.toggleSidebar = false;
    }

  }
}
