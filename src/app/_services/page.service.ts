import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({ providedIn: 'root' })
export class PageService {
    constructor(public http: HttpClient) { }

    public getAll(filter = {params:{}}) {
        return this.http.get<any>(`page/list`, filter);
    }

    getById(id: number) {
        return this.http.get(`page/edit/` + id);
    }

    insert(page: any) {
        return this.http.post(`page/add`, page);
    }

    update(id:number, page: any) {
        return this.http.post(`page/update/` + id, page);
    }

    delete(id: number) {
        return this.http.get(`page/delete/` + id);
    }

    faqinsert(data: any) {
        return this.http.post(`page/faq_add`, data);
    }

    getFaqs() {
        return this.http.get(`page/faqs`);
    }
}
