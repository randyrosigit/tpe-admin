export class Customer {
    customer_id: number;
    customer_firstname: string;
    customer_lastname: string;
    customer_email: string;
    customer_status: string;
    customer_created_on: string;
    status: string;
    message: string;
    result_set: Array<any>;
}