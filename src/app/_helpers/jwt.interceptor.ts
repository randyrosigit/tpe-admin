import { Injectable } from '@angular/core';
import { HttpRequest, HttpHandler, HttpEvent, HttpInterceptor, HttpResponse, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';

import { AuthenticationService } from '../_services';

@Injectable()
export class JwtInterceptor implements HttpInterceptor {
    constructor(private authenticationService: AuthenticationService) {}

    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        // add authorization header with jwt token if available
        let currentUser = this.authenticationService.currentUserValue;
        if (currentUser && currentUser['token']) {
            request = request.clone({
                /*setHeaders: { 
                    Auth: `Bearer:${currentUser.token}`
                }*/
                headers: new HttpHeaders({
                  'Auth': `Bearer:${currentUser['token']}`
                })
            });
        }

        request = request.clone({ 
            //url: `http://pickyadmin.ezzysales.com/webservice/adminapi/${request.url}` 
          //url: `http://teamworks/team1/picky/adminapi/${request.url}`
           url:  `https://api.thepicky.co/index.php/adminapi/${request.url}`
        });

        // return next.handle(request);
        return next.handle(request);
    }
}