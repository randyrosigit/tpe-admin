import { Component, OnInit, OnDestroy, ViewChild } from '@angular/core';
import { Subscription } from 'rxjs';
import { first } from 'rxjs/operators';
import {MatTableDataSource, MatSort, MatPaginator} from '@angular/material';
import {SelectionModel} from '@angular/cdk/collections';
import { ngxCsv } from 'ngx-csv/ngx-csv';

// import { Inventory } from '../../_models';
import { AlertService, InventoryService, MerchantService, CustomerService } from '../../_services';

@Component({ templateUrl: 'inventory.list.html' })
export class InventoryList implements OnInit, OnDestroy {
    
    heading = 'Orders';
	subheading = '';
	icon = 'pe-7s-cash icon-gradient bg-happy-itmeo';
    inventorys: [];
    merchants = [];
    customers = [];
    merchant_id = null;
    customer_id = null;
    dataSource: MatTableDataSource<any>;
    renderedData: any;
    pageSizeOptions = null;

    displayedColumns = ['s_no','voucher_name', 'customer_name', 'merchant_name', 'inventory_type', 'voucher_value', 'voucher_expiry_date', 'inventory_created_on', 'inventory_status'];
    

    @ViewChild(MatPaginator) paginator: MatPaginator;  
    @ViewChild(MatSort) sort: MatSort;
    
    setPageSizeOptions(setPageSizeOptionsInput: string) {
      this.pageSizeOptions = setPageSizeOptionsInput.split(',').map(str => +str);
    }

    constructor( 
        private inventoryService: InventoryService,
        private alertService: AlertService,
        private merchantServices: MerchantService,
        private customerService: CustomerService
    ) { }

    ngOnInit() {
        this.loadAllInventory();
        this.loadAllMerchants();
        this.loadAllCustomers();
    }

    ngOnDestroy() {
    }

    private loadAllMerchants() {
        this.merchantServices.getAll().pipe(first()).subscribe((merchants: any) => {
            if(merchants.status=='ok') {
                this.merchants = merchants.result_set;
            }
        });
    }

    private loadAllCustomers() {
        this.customerService.getAll().pipe(first()).subscribe((customers: any) => {
            if(customers.status=='ok') {
                this.customers = customers.result_set;
            }
        });
    }

    private loadAllInventory() {
        this.inventoryService.getAll().pipe(first()).subscribe((inventorys: any) => {
        	if(inventorys.status=='ok') {
                this.dataSource = new MatTableDataSource(inventorys.result_set);
                this.dataSource.paginator = this.paginator;  
                this.dataSource.sort = this.sort; 
                this.inventorys = inventorys.result_set;
        	}
        });
    }

    private Filter() {
        this.inventoryService.getAll({ params: { filter_merchant_id: this.merchant_id, filter_customer_id: this.customer_id } }).pipe(first()).subscribe((inventorys: any) => {
            if(inventorys.status=='ok') {
                this.dataSource = new MatTableDataSource(inventorys.result_set);
                this.dataSource.paginator = this.paginator;  
                this.dataSource.sort = this.sort; 
                this.inventorys = inventorys.result_set;
            }
        });
    }

    private Reset() {
        this.merchant_id ='';
        this.customer_id ='';
        this.loadAllInventory();
    }

    ExportCSV() {
      var options = { 
        fieldSeparator: ',',
        quoteStrings: '"',
        decimalseparator: '.',
        showLabels: true, 
        showTitle: false,
        title: 'Orders',
        useBom: true,
        noDownload: false,
        headers: ["Voucher", "Order Date", "Voucher Value", "Merchant", "Order Type", "Order Status", "Expiry Date", "Customer Name"]
      };
     
      new ngxCsv(this.inventorys, 'My Report', options);
    }

    applyFilter(filterValue: string) {  
      this.dataSource.filter = filterValue.trim().toLowerCase();  
    
      if (this.dataSource.paginator) {  
        this.dataSource.paginator.firstPage();  
      }  
    }

}
