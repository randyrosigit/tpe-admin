import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { first } from 'rxjs/operators';

import { MustMatch } from '../../_helpers/must-match.validator';
import { AlertService, MerchantService, CustomerService } from '../../_services';
import { Utils } from '../../_helpers';

@Component({templateUrl: 'customer.edit.html'})
export class CustomerEdit implements OnInit {
    heading = 'Edit Customer';
    subheading = '';
    icon = 'fa fa-users icon-gradient bg-happy-itmeo';
    customerForm: FormGroup;
    loading = false;
    submitted = false;
    customer_profile = null;
    merchants = [];
    preview_customer_profile = null;
    customer_id: number;

    constructor(
        private formBuilder: FormBuilder,
        private router: Router,
        private route: ActivatedRoute,
        private alertService: AlertService,
        private merchantServices: MerchantService,
        private customerService: CustomerService        
    ) {  
        this.customer_id = this.route.snapshot.params['id'];
    }

    ngOnInit() {
        this.getCustomer(this.customer_id);
        this.customerForm = this.formBuilder.group({
            customer_firstname: ['', [Validators.required, Utils.noWhitespaceValidator]],
            customer_lastname: ['', [Validators.required, Utils.noWhitespaceValidator]],
            customer_email: ['', [Validators.required, Utils.noWhitespaceValidator, Validators.pattern('[a-zA-Z0-9.-]{1,}@[a-zA-Z.-]{2,}[.]{1}[a-zA-Z]{2,}')]],
            customer_password: [''],
            customer_confirm_password: [''],
            customer_profile: ['', [Validators.required, Utils.noWhitespaceValidator]],
            customer_status: ['', [Validators.required, Utils.noWhitespaceValidator]]
            /*customer_firstname: ['', Validators.required],
            customer_lastname: ['', Validators.required],
            customer_email: ['', [Validators.required, Validators.pattern('[a-zA-Z0-9.-]{1,}@[a-zA-Z.-]{2,}[.]{1}[a-zA-Z]{2,}')]],
            customer_password: [''],
            customer_confirm_password: [''],
            customer_profile: ['', Validators.required],
            customer_status: ['', Validators.required]*/
        }, {
            validator: MustMatch('customer_password', 'customer_confirm_password')
        });
    }

    // convenience getter for easy access to form fields
    get f() { return this.customerForm.controls; }

    getCustomer(id) {
        this.customerService.getById(id).subscribe((data: any) => {
            var customer = data.result_set;
            this.customerForm.setValue({
              customer_firstname: customer.customer_firstname,
              customer_lastname: customer.customer_lastname,
              customer_email: customer.customer_email,
              customer_password: '',
              customer_confirm_password: '',
              customer_profile: customer.customer_profile,
              customer_status: customer.customer_status
            });
            this.preview_customer_profile = customer.customer_profile;
        });
    }

    IconChange(files: FileList) {
        this.customer_profile = files[0];
        this.customerForm.get('customer_profile').setValue(this.customer_profile.name);
        var reader = new FileReader();
        reader.readAsDataURL(this.customer_profile); 
        reader.onload = (_event) => { 
          this.preview_customer_profile = reader.result; 
        }
    }

    onSubmit() {
        this.submitted = true;

        // stop here if form is invalid
        if (this.customerForm.invalid) {
            return;
        }

        this.loading = true;

        const formData = new FormData();
        formData.append('customer_firstname', this.customerForm.get('customer_firstname').value);
        formData.append('customer_lastname', this.customerForm.get('customer_lastname').value);
        formData.append('customer_email', this.customerForm.get('customer_email').value);
        formData.append('customer_password', this.customerForm.get('customer_password').value);
        formData.append('customer_status', this.customerForm.get('customer_status').value);

        if(this.customer_profile!=null) {
          formData.append('customer_profile', this.customer_profile, this.customer_profile.name);
        }

        this.customerService.update(this.customer_id, formData).pipe(first()).subscribe((data: any) => {
              if(data.status=='ok') {
                this.alertService.success(data.message, true);
                this.router.navigate(['/customer']);
              } else {
                this.alertService.error(data.message, true);
              }
            },
            error => {
                this.alertService.error(error);
                this.loading = false;
            }
        );
    }
}