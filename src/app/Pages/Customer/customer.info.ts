import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { first } from 'rxjs/operators';

import { AlertService, CustomerService } from '../../_services';

@Component({templateUrl: 'customer.info.html'})
export class CustomerInfo implements OnInit {
    heading = 'Customer Information';
    subheading = '';
    icon = 'fa fa-users icon-gradient bg-happy-itmeo';
    customer_id = '';
    info: any;

    constructor(
        private router: Router,
        private route: ActivatedRoute,
        private alertService: AlertService,
        private customerService: CustomerService        
    ) {  
        this.customer_id = this.route.snapshot.params['id'];
    }

    ngOnInit() {
        this.getCustomerInfo(this.customer_id);
    }

    getCustomerInfo(id) {
        this.customerService.getInfoById(id).subscribe((data: any) => {
            if(data.status=='ok') {
                this.info = data.result_set;  
            }                      
        });
    }

}