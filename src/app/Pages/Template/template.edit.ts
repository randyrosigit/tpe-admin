import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { first } from 'rxjs/operators';
import {AngularEditorConfig} from '@kolkov/angular-editor';

import { AlertService, TemplateService, CategoryService } from '../../_services';
import { Utils } from '../../_helpers';

@Component({templateUrl: 'template.edit.html'})
export class TemplateEdit implements OnInit {
    heading = 'Edit Template';
    subheading = '';
    icon = 'fa fa-clone icon-gradient bg-happy-itmeo';
    templateForm: FormGroup;
    loading = false;
    submitted = false;
    template_id: number;

    wyswigconfig: AngularEditorConfig = {
      editable: true,
      spellcheck: true,
      height: '15rem',
      minHeight: '5rem',
      placeholder: 'Enter text here...',
      translate: 'no',
      customClasses: [
        {
          name: 'quote',
          class: 'quote',
        },
        {
          name: 'redText',
          class: 'redText'
        },
        {
          name: 'titleText',
          class: 'titleText',
          tag: 'h1',
        },
      ]
    };

    constructor(
        private formBuilder: FormBuilder,
        private router: Router,
        private route: ActivatedRoute,
        private templateService: TemplateService,
        private alertService: AlertService
    ) { 
        this.template_id = this.route.snapshot.params['id'];
    }

    ngOnInit() {
        this.getTemplate(this.template_id);
        this.templateForm = this.formBuilder.group({
            template_name: ['', [Validators.required, Utils.noWhitespaceValidator]],
            template_description: ['', [Validators.required, Utils.noWhitespaceValidator]]
        });
    }

    // convenience getter for easy access to form fields
    get f() { return this.templateForm.controls; }

    getTemplate(id) {
        this.templateService.getById(id).subscribe((data: any) => {
            var template = data.result_set;
            this.templateForm.setValue({
              template_name: template.template_name,
              template_description: template.template_description
            });
        });
    }

    onSubmit() {
        this.submitted = true;

        // stop here if form is invalid
        if (this.templateForm.invalid) {
            return;
        }

        this.loading = true;

        const formData = new FormData();
        formData.append('template_name', this.templateForm.get('template_name').value);
        formData.append('template_description', this.templateForm.get('template_description').value);
        
        this.templateService.update(this.template_id, formData).pipe(first()).subscribe((data: any) => {
              if(data.status=='ok') {
                this.alertService.success(data.message, true);
                this.router.navigate(['/template']);
              } else {
                this.alertService.error(data.message, true);
              }
            },
            error => {
                this.alertService.error(error);
                this.loading = false;
            }
        );
    }
}