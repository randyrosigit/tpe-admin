import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { first } from 'rxjs/operators';
import {AngularEditorConfig} from '@kolkov/angular-editor';

import { AlertService, TemplateService } from '../../_services';
import { Utils } from '../../_helpers';

@Component({templateUrl: 'template.add.html'})
export class TemplateAdd implements OnInit {
    heading = 'Add Template';
    subheading = '';
    icon = 'fa fa-clone icon-gradient bg-happy-itmeo';
    templateForm: FormGroup;
    loading = false;
    submitted = false;

    wyswigconfig: AngularEditorConfig = {
      editable: true,
      spellcheck: true,
      height: '15rem',
      minHeight: '5rem',
      placeholder: 'Enter text here...',
      translate: 'no',
      customClasses: [
        {
          name: 'quote',
          class: 'quote',
        },
        {
          name: 'redText',
          class: 'redText'
        },
        {
          name: 'titleText',
          class: 'titleText',
          tag: 'h1',
        },
      ]
    };

    constructor(
        private formBuilder: FormBuilder,
        private router: Router,
        private templateService: TemplateService,
        private alertService: AlertService
    ) { }

    ngOnInit() {
        this.templateForm = this.formBuilder.group({
            template_name: ['', [Validators.required, Utils.noWhitespaceValidator]],
            template_description: ['', Validators.required]
        });
    }

    // convenience getter for easy access to form fields
    get f() { return this.templateForm.controls; }

    onSubmit() {
        this.submitted = true;

        // stop here if form is invalid
        if (this.templateForm.invalid) {
            return;
        }

        this.loading = true;

        const formData = new FormData();
        formData.append('template_name', this.templateForm.get('template_name').value);
        formData.append('template_description', this.templateForm.get('template_description').value);

        this.templateService.insert(formData).pipe(first()).subscribe((data: any) => {
              if(data.status=='ok') {
                this.alertService.success(data.message, true);
                this.router.navigate(['/template']);
              } else {
                this.alertService.error(data.message, true);
              }
            },
            error => {
                this.alertService.error(error);
                this.loading = false;
            }
        );
    }
}