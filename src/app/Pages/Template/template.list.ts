import { Component, OnInit, OnDestroy, ViewChild } from '@angular/core';
import { Subscription } from 'rxjs';
import { first } from 'rxjs/operators';
import {MatTableDataSource, MatSort, MatPaginator} from '@angular/material';
import {SelectionModel} from '@angular/cdk/collections';

import { AlertService, TemplateService } from '../../_services';

@Component({ templateUrl: 'template.list.html' })
export class TemplateList implements OnInit, OnDestroy {
    
    heading = 'Email Templates';
    subheading = '';
    icon = 'fa fa-clone icon-gradient bg-happy-itmeo';
    // link = '/template/add';
    dataSource: MatTableDataSource<any[]>;
    search_key = "";
    pageSizeOptions = null;

    displayedColumns = [ 's_no','template_name', 'template_created_on', 'actions'];
    

    @ViewChild(MatPaginator) paginator: MatPaginator;  
    @ViewChild(MatSort) sort: MatSort;
    
    setPageSizeOptions(setPageSizeOptionsInput: string) {
      this.pageSizeOptions = setPageSizeOptionsInput.split(',').map(str => +str);
    }

    constructor( 
        private templateService: TemplateService,
        private alertService: AlertService ) { }

    ngOnInit() {
        this.loadAllTemplate();
    }

    ngOnDestroy() {
    }

    clearFilters(){
       this.dataSource.filter = '';
       this.search_key = '';
    }

    private loadAllTemplate() {
        this.templateService.getAll().pipe(first()).subscribe((templates: any) => {
        	if(templates.status=='ok') {
            this.dataSource = new MatTableDataSource(templates.result_set);
            this.dataSource.paginator = this.paginator;  
            this.dataSource.sort = this.sort; 
        	}
        });
    }

    deleteTemplate(id: number) {
        this.alertService.delete().then(data=>{
            if(data) {
                this.templateService.delete(id).pipe(first()).subscribe((data: any) => {
                    if(data.status=='ok') {
                      this.alertService.success(data.message, true);
                      this.loadAllTemplate()
                    } else {
                      this.alertService.error(data.message, true);
                    }
                });
            } 
        });
    }

    applyFilter(filterValue: string) {  
      this.dataSource.filter = filterValue.trim().toLowerCase();  
    
      if (this.dataSource.paginator) {  
        this.dataSource.paginator.firstPage();  
      }  
    }

}
