import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';
import { first } from 'rxjs/operators';

// import { User } from '../../_models';
import { UserService, AuthenticationService, HomeService } from '../../_services';

@Component({ templateUrl: 'home.component.html' })
export class HomeComponent implements OnInit, OnDestroy {
    heading = 'Dashboard';
    subheading = '';
    icon = 'fa fa-dashboard';
    currentUser = null;
    currentUserSubscription: Subscription;
    home_data: any;
  
    constructor(
        private userService: UserService,
        private authenticationService: AuthenticationService,
        private homeService: HomeService
    ) {
        this.currentUserSubscription = this.authenticationService.currentUser.subscribe((user: any) => {
            this.currentUser = user;
        });
    } 

    ngOnInit() {
        this.loadDashboard();
    }

    ngOnDestroy() {
        // unsubscribe to ensure no memory leaks
        // this.currentUserSubscription.unsubscribe();
    }

    private loadDashboard() {
        this.homeService.getAll().pipe(first()).subscribe((data: any) => {
            if(data.status=='ok') {
                this.home_data = data.result_set;
            }            
        });
    } 
}