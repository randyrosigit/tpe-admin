import { Component, OnInit, OnDestroy, ViewChild } from '@angular/core';
import { Subscription } from 'rxjs';
import { first } from 'rxjs/operators';
import {MatTableDataSource, MatSort, MatPaginator} from '@angular/material';
import {SelectionModel} from '@angular/cdk/collections';
import { ngxCsv } from 'ngx-csv/ngx-csv';

import { AlertService, OrderService, MerchantService, CustomerService } from '../../_services';

@Component({ templateUrl: 'order.list.html' })
export class OrderList implements OnInit, OnDestroy {
    
    heading = 'Orders';
	subheading = '';
	icon = 'fa fa-money icon-gradient bg-happy-itmeo';
    orders: [];
    merchants = [];
    customers = [];
    merchant_id = null;
    customer_id = null;
    dataSource: MatTableDataSource<any>;
    renderedData: any;
    pageSizeOptions = null;

    displayedColumns = ['s_no', 'order_ref_id', 'customer_name', 'order_total', 'order_payment_method', 'order_status', 'order_created_on', 'actions'];

    @ViewChild(MatPaginator) paginator: MatPaginator;  
    @ViewChild(MatSort) sort: MatSort;
    
    setPageSizeOptions(setPageSizeOptionsInput: string) {
      this.pageSizeOptions = setPageSizeOptionsInput.split(',').map(str => +str);
    }

    constructor( 
        private orderService: OrderService,
        private alertService: AlertService,
        private merchantServices: MerchantService,
        private customerService: CustomerService
    ) { }

    ngOnInit() {
        this.loadAllOrders();
        this.loadAllMerchants();
        this.loadAllCustomers();
    }

    ngOnDestroy() {
    }

    private loadAllMerchants() {
        this.merchantServices.getAll().pipe(first()).subscribe((merchants: any) => {
            if(merchants.status=='ok') {
                this.merchants = merchants.result_set;
            }
        });
    }

    private loadAllCustomers() {
        this.customerService.getAll().pipe(first()).subscribe((customers: any) => {
            if(customers.status=='ok') {
                this.customers = customers.result_set;
            }
        });
    }

    private loadAllOrders() {
        this.orderService.getAll().pipe(first()).subscribe((orders: any) => {
        	if(orders.status=='ok') {
                this.dataSource = new MatTableDataSource(orders.result_set);
                this.dataSource.paginator = this.paginator;  
                this.dataSource.sort = this.sort; 
                this.orders = orders.result_set;
        	}
        });
    }

    private Filter() {
        this.orderService.getAll({ params: { filter_merchant_id: this.merchant_id, filter_customer_id: this.customer_id } }).pipe(first()).subscribe((orders: any) => {
            if(orders.status=='ok') {
                this.dataSource = new MatTableDataSource(orders.result_set);
                this.dataSource.paginator = this.paginator;  
                this.dataSource.sort = this.sort; 
                this.orders = orders.result_set;
            }
        });
    }

    private Reset() {
        this.merchant_id =null;
        this.customer_id =null;
        this.loadAllOrders();
    }

    ExportCSV() {
      var options = { 
        fieldSeparator: ',',
        quoteStrings: '"',
        decimalseparator: '.',
        showLabels: true, 
        showTitle: false,
        title: 'Orders',
        useBom: true,
        noDownload: false,
        headers: ["Order ID", "Order Reference ID", "Total Amount", "Payment Mode", "Payment Method", "Order Status", "Order Date", "Customer Name"]
      };
     
      new ngxCsv(this.orders, 'My Report', options);
    }

    applyFilter(filterValue: string) {  
      this.dataSource.filter = filterValue.trim().toLowerCase();  
    
      if (this.dataSource.paginator) {  
        this.dataSource.paginator.firstPage();  
      }  
    }

}
