import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { first } from 'rxjs/operators';

import { AlertService, OrderService } from '../../_services';
import { Utils } from '../../_helpers';

@Component({templateUrl: 'order.view.html'})
export class OrderView implements OnInit {
    heading = 'Order Details';
    subheading = '';
    icon = 'fa fa-money icon-gradient bg-happy-itmeo';
    orderForm: FormGroup;
    order_id: number;
    order_info = [];

    constructor(
        private formBuilder: FormBuilder,
        private router: Router,
        private route: ActivatedRoute,
        private alertService: AlertService,
        private orderService: OrderService        
    ) {  
        this.order_id = this.route.snapshot.params['id'];
    }

    ngOnInit() {
        this.getOrder(this.order_id);
        this.orderForm = this.formBuilder.group({
        });
    }

    // convenience getter for easy access to form fields
    get f() { return this.orderForm.controls; }

    getOrder(id) {
        this.orderService.getById(id).subscribe((data: any) => {
            var order = data.result_set;
            this.order_info = order;
        });
    }


}