import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { first } from 'rxjs/operators';
import {AngularEditorConfig} from '@kolkov/angular-editor';

import { AlertService, CategoryService } from '../../_services';
import { Utils } from '../../_helpers';

@Component({templateUrl: 'category.edit.html'})
export class CategoryEdit implements OnInit {
    heading = 'Edit Category';
    subheading = '';
    icon = 'fa fa-folder-o icon-gradient bg-happy-itmeo';
    categoryForm: FormGroup;
    loading = false;
    submitted = false;
    category_id: number;
    category_image = null;
    preview_category_image = null;

    constructor(
        private formBuilder: FormBuilder,
        private router: Router,
        private route: ActivatedRoute,
        private categoryService: CategoryService,
        private alertService: AlertService
    ) { 
        this.category_id = this.route.snapshot.params['id'];
    }

    ngOnInit() {
        this.getCategory(this.category_id);
        this.categoryForm = this.formBuilder.group({
            category_name: ['', [Validators.required, Utils.noWhitespaceValidator]],
            category_image: ['', [Validators.required, Utils.noWhitespaceValidator]],
            category_status: ['', [Validators.required, Utils.noWhitespaceValidator]]
        });
    }

    IconChange(files: FileList) {
        this.category_image = files[0];
        this.categoryForm.get('category_image').setValue(this.category_image.name);
        var reader = new FileReader();
        reader.readAsDataURL(this.category_image); 
        reader.onload = (_event) => { 
          this.preview_category_image = reader.result; 
        }
    }

    // convenience getter for easy access to form fields
    get f() { return this.categoryForm.controls; }

    getCategory(id) {
        this.categoryService.getById(id).subscribe((data: any) => {
            var category = data.result_set;
            this.categoryForm.setValue({
              category_name: category.category_name,
              category_status: category.category_status,
              category_image: category.category_image
            });
            this.preview_category_image = category.category_image;
        });
    }

    onSubmit() {
        this.submitted = true;

        // stop here if form is invalid
        if (this.categoryForm.invalid) {
            return;
        }

        this.loading = true;

        const formData = new FormData();
        formData.append('category_name', this.categoryForm.get('category_name').value);
        formData.append('category_status', this.categoryForm.get('category_status').value);
        
        if(this.category_image!=null) {
          formData.append('category_image', this.category_image, this.category_image.name);
        }

        this.categoryService.update(this.category_id, formData).pipe(first()).subscribe((data: any) => {
              if(data.status=='ok') {
                this.alertService.success(data.message, true);
                this.router.navigate(['/category']);
              } else {
                this.alertService.error(data.message, true);
              }
            },
            error => {
                this.alertService.error(error);
                this.loading = false;
            }
        );
    }
}