import { Component, OnInit, OnDestroy, ViewChild } from '@angular/core';
import { Subscription } from 'rxjs';
import { first } from 'rxjs/operators';
import {MatTableDataSource, MatSort, MatPaginator} from '@angular/material';
import {SelectionModel} from '@angular/cdk/collections';

import { AlertService, RoleService } from '../../_services';

@Component({ templateUrl: 'role.list.html' })
export class RoleList implements OnInit, OnDestroy {
    
    heading = 'User Roles';
    subheading = '';
    icon = 'fa fa-clone icon-gradient bg-happy-itmeo';
    link = '/role/add';
    dataSource: MatTableDataSource<any[]>;
    search_key = "";
    pageSizeOptions = null;

    displayedColumns = [ 's_no','roles_title', 'privilege', 'actions'];
    

    @ViewChild(MatPaginator) paginator: MatPaginator;  
    @ViewChild(MatSort) sort: MatSort;
    
    setPageSizeOptions(setPageSizeOptionsInput: string) {
      this.pageSizeOptions = setPageSizeOptionsInput.split(',').map(str => +str);
    }

    constructor( 
        private roleService: RoleService,
        private alertService: AlertService ) { }

    ngOnInit() {
        this.loadAllRole();
    }

    ngOnDestroy() {
    }

    clearFilters(){
       this.dataSource.filter = '';
       this.search_key = '';
    }

    private loadAllRole() {
        this.roleService.getAll().pipe(first()).subscribe((roles: any) => {
        	if(roles.status=='ok') {
            this.dataSource = new MatTableDataSource(roles.result_set);
            this.dataSource.paginator = this.paginator;  
            this.dataSource.sort = this.sort; 
        	}
        });
    }

    deleteRole(id: number) {
        this.alertService.delete().then(data=>{
            if(data) {
                this.roleService.delete(id).pipe(first()).subscribe((data: any) => {
                    if(data.status=='ok') {
                      this.alertService.success(data.message, true);
                      this.loadAllRole()
                    } else {
                      this.alertService.error(data.message, true);
                    }
                });
            } 
        });
    }

    applyFilter(filterValue: string) {  
      this.dataSource.filter = filterValue.trim().toLowerCase();  
    
      if (this.dataSource.paginator) {  
        this.dataSource.paginator.firstPage();  
      }  
    }

}
