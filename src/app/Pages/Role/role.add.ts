import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, FormControl, Validators, FormArray } from '@angular/forms';
import { first } from 'rxjs/operators';
import {AngularEditorConfig} from '@kolkov/angular-editor';

import { AlertService, RoleService } from '../../_services';
import { Utils } from '../../_helpers';

@Component({templateUrl: 'role.add.html'})
export class RoleAdd implements OnInit {
    heading = 'Add Role';
    subheading = '';
    icon = 'fa fa-clone icon-gradient bg-happy-itmeo';
    roleForm: FormGroup;
    loading = false;
    submitted = false;
    showDiv = false;

    wyswigconfig: AngularEditorConfig = {
      editable: true,
      spellcheck: true,
      height: '15rem',
      minHeight: '5rem',
      placeholder: 'Enter text here...',
      translate: 'no',
      customClasses: [
        {
          name: 'quote',
          class: 'quote',
        },
        {
          name: 'redText',
          class: 'redText'
        },
        {
          name: 'titleText',
          class: 'titleText',
          tag: 'h1',
        },
      ]
    };

    constructor(
        private formBuilder: FormBuilder,
        private router: Router,
        private roleService: RoleService,
        private alertService: AlertService
    ) { }

    users: any;
    roles: any;
    categorys: any;
    merchants: any;
    vouchers: any;
    redeem_requests: any;
    quests: any;
    complete_requests: any;
    polls: any;
    orders: any;
    customers: any;
    pages: any;
    settings: any;
    faqs: any;
    templates: any;

    ngOnInit() {
        this.roleForm = this.formBuilder.group({
            roles_title: ['', [Validators.required, Utils.noWhitespaceValidator]],
            privilege: ['all', Validators.required],
            roles: this.formBuilder.array([])
        });
        this.PrivilegeChange();

        this.users = [{'title': 'Admin User Add', 'value': '/user/add', 'checked': false, 'disabled': false},{'title': 'Admin User Edit', 'value': '/user/edit', 'checked': false, 'disabled': false},{'title': 'Admin User Delete', 'value': '/user/delete', 'checked': false, 'disabled': false}];
        this.roles = [{'title': 'Admin Role Add', 'value': '/role/add', 'checked': false, 'disabled': false},{'title': 'Admin Role Edit', 'value': '/role/edit', 'checked': false, 'disabled': false},{'title': 'Admin Role Delete', 'value': '/role/delete', 'checked': false, 'disabled': false}];
        this.categorys = [{'title': 'Category Add', 'value': '/category/add', 'checked': false, 'disabled': false},{'title': 'Category Edit', 'value': '/category/edit', 'checked': false, 'disabled': false},{'title': 'Category Delete', 'value': '/category/delete', 'checked': false, 'disabled': false}];
        this.merchants = [{'title': 'Merchant Add', 'value': '/merchant/add', 'checked': false, 'disabled': false},{'title': 'Merchant Edit', 'value': '/merchant/edit', 'checked': false, 'disabled': false},{'title': 'Merchant Delete', 'value': '/merchant/delete', 'checked': false, 'disabled': false}];
        this.vouchers = [{'title': 'Voucher Add', 'value': '/voucher/add', 'checked': false, 'disabled': false},{'title': 'Voucher Edit', 'value': '/voucher/edit', 'checked': false, 'disabled': false},{'title': 'Voucher Delete', 'value': '/voucher/delete', 'checked': false, 'disabled': false}];
        this.redeem_requests = [{'title': 'Voucher Redeem Request', 'value': '/redeem_requests/', 'checked': false, 'disabled': false}];
        this.quests = [{'title': 'Quest Add', 'value': '/quest/add', 'checked': false, 'disabled': false},{'title': 'Quest Edit', 'value': '/quest/edit', 'checked': false, 'disabled': false},{'title': 'Quest Delete', 'value': '/quest/delete', 'checked': false, 'disabled': false}];
        this.complete_requests = [{'title': 'Quest Complete Requests', 'value': '/complete_requests/', 'checked': false, 'disabled': false}];
        this.polls = [{'title': 'Poll Add', 'value': '/poll/add', 'checked': false, 'disabled': false},{'title': 'Poll Edit', 'value': '/poll/edit', 'checked': false, 'disabled': false},{'title': 'Poll Delete', 'value': '/poll/delete', 'checked': false, 'disabled': false}];
        this.orders = [{'title': 'Order', 'value': '/orders/', 'checked': false, 'disabled': false}];
        this.customers = [{'title': 'Customer Add', 'value': '/customer/add', 'checked': false, 'disabled': false},{'title': 'Customer Edit', 'value': '/customer/edit', 'checked': false, 'disabled': false},{'title': 'Cusotmer Delete', 'value': '/customer/delete', 'checked': false, 'disabled': false}];
        this.pages = [{'title': 'Page Add', 'value': '/page/add', 'checked': false, 'disabled': false},{'title': 'Page Edit', 'value': '/page/edit', 'checked': false, 'disabled': false},{'title': 'Page Delete', 'value': '/page/delete', 'checked': false, 'disabled': false}];
        this.settings = [{'title': 'Settings', 'value': '/settings/', 'checked': false, 'disabled': false}];
        this.faqs = [{'title': 'FAQs', 'value': '/faqs/', 'checked': false, 'disabled': false}];
        this.templates = [{'title': 'Email Template Add', 'value': '/template/add', 'checked': false, 'disabled': false},{'title': 'Email Template Edit', 'value': '/template/edit', 'checked': false, 'disabled': false},{'title': 'Email Template Delete', 'value': '/template/delete', 'checked': false, 'disabled': false}];
    }

    /*onChange(e) {
      this.users[0].checked = true;
      console.log(e);
    }*/

    /*onModuleChange(event, module) {
      const roles = <FormArray>this.roleForm.get('roles') as FormArray;

      if(event.checked) {
        roles.push(new FormControl(event.source.value))
      } else {
        const i = roles.controls.findIndex(x => x.value === event.source.value);
        roles.removeAt(i);
      }
    }*/

    onChange(event) {
      const roles = <FormArray>this.roleForm.get('roles') as FormArray;

      if(event.checked) {
        roles.push(new FormControl(event.source.value))
      } else {
        const i = roles.controls.findIndex(x => x.value === event.source.value);
        roles.removeAt(i);
      }
    }

    PrivilegeChange() {
      this.roleForm.get('privilege').valueChanges.subscribe((Privilege: any) => {
        if(Privilege=="custom") {
          this.showDiv = true;
        } else {
          this.showDiv = false;        
        }
      });
    }

    // convenience getter for easy access to form fields
    get f() { return this.roleForm.controls; }

    onSubmit() {
        this.submitted = true;

        // stop here if form is invalid
        if (this.roleForm.invalid) {
            return;
        }

        this.loading = true;

        const formData = new FormData();
        formData.append('roles_title', this.roleForm.get('roles_title').value);
        formData.append('privilege', this.roleForm.get('privilege').value);
        formData.append('rules', JSON.stringify(this.roleForm.get('roles').value));

        this.roleService.insert(formData).pipe(first()).subscribe((data: any) => {
          if(data.status=='ok') {
            this.alertService.success(data.message, true);
            this.router.navigate(['/role']);
          } else {
            this.alertService.error(data.message, true);
          }
        }, error => {
          this.alertService.error(error);
          this.loading = false;
        });
    }
}