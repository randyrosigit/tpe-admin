import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, FormControl, Validators, FormArray } from '@angular/forms';
import { first } from 'rxjs/operators';
import {AngularEditorConfig} from '@kolkov/angular-editor';

import { AlertService, RoleService, CategoryService } from '../../_services';
import { Utils } from '../../_helpers';

@Component({templateUrl: 'role.edit.html'})
export class RoleEdit implements OnInit {
    heading = 'Edit Role';
    subheading = '';
    icon = 'fa fa-clone icon-gradient bg-happy-itmeo';
    roleForm: FormGroup;
    loading = false;
    submitted = false;
    role_id: number;
    showDiv = false;
    permissions: any;

    wyswigconfig: AngularEditorConfig = {
      editable: true,
      spellcheck: true,
      height: '15rem',
      minHeight: '5rem',
      placeholder: 'Enter text here...',
      translate: 'no',
      customClasses: [
        {
          name: 'quote',
          class: 'quote',
        },
        {
          name: 'redText',
          class: 'redText'
        },
        {
          name: 'titleText',
          class: 'titleText',
          tag: 'h1',
        },
      ]
    };

    constructor(
        private formBuilder: FormBuilder,
        private router: Router,
        private route: ActivatedRoute,
        private roleService: RoleService,
        private alertService: AlertService
    ) { 
        this.role_id = this.route.snapshot.params['id'];
    }

    ngOnInit() {
        this.getRole(this.role_id);
        this.roleForm = this.formBuilder.group({
            roles_title: ['', [Validators.required, Utils.noWhitespaceValidator]],
            privilege: ['all', Validators.required],
            roles: this.formBuilder.array([])
        });

        this.PrivilegeChange();

        this.permissions =   {
          "users": [{'title': 'Admin User Add', 'value': '/user/add', 'checked': false, 'disabled': false},{'title': 'Admin User Edit', 'value': '/user/edit', 'checked': false, 'disabled': false},{'title': 'Admin User Delete', 'value': '/user/delete', 'checked': false, 'disabled': false}],
          "roles": [{'title': 'Admin Role Add', 'value': '/role/add', 'checked': false, 'disabled': false},{'title': 'Admin Role Edit', 'value': '/role/edit', 'checked': false, 'disabled': false},{'title': 'Admin Role Delete', 'value': '/role/delete', 'checked': false, 'disabled': false}],
          "categorys": [{'title': 'Category Add', 'value': '/category/add', 'checked': false, 'disabled': false},{'title': 'Category Edit', 'value': '/category/edit', 'checked': false, 'disabled': false},{'title': 'Category Delete', 'value': '/category/delete', 'checked': false, 'disabled': false}],
          "merchants": [{'title': 'Merchant Add', 'value': '/merchant/add', 'checked': false, 'disabled': false},{'title': 'Merchant Edit', 'value': '/merchant/edit', 'checked': false, 'disabled': false},{'title': 'Merchant Delete', 'value': '/merchant/delete', 'checked': false, 'disabled': false}],
          "vouchers": [{'title': 'Voucher Add', 'value': '/voucher/add', 'checked': false, 'disabled': false},{'title': 'Voucher Edit', 'value': '/voucher/edit', 'checked': false, 'disabled': false},{'title': 'Voucher Delete', 'value': '/voucher/delete', 'checked': false, 'disabled': false}],
          "redeem_requestss": [{'title': 'Voucher Redeem Request', 'value': '/redeem_requests/', 'checked': false, 'disabled': false}],
          "quests": [{'title': 'Quest Add', 'value': '/quest/add', 'checked': false, 'disabled': false},{'title': 'Quest Edit', 'value': '/quest/edit', 'checked': false, 'disabled': false},{'title': 'Quest Delete', 'value': '/quest/delete', 'checked': false, 'disabled': false}],
          "complete_requestss": [{'title': 'Quest Complete Requests', 'value': '/complete_requests/', 'checked': false, 'disabled': false}],
          "polls": [{'title': 'Poll Add', 'value': '/poll/add', 'checked': false, 'disabled': false},{'title': 'Poll Edit', 'value': '/poll/edit', 'checked': false, 'disabled': false},{'title': 'Poll Delete', 'value': '/poll/delete', 'checked': false, 'disabled': false}],
          "orderss": [{'title': 'Order', 'value': '/orders/', 'checked': false, 'disabled': false}],
          "customers": [{'title': 'Customer Add', 'value': '/customer/add', 'checked': false, 'disabled': false},{'title': 'Customer Edit', 'value': '/customer/edit', 'checked': false, 'disabled': false},{'title': 'Cusotmer Delete', 'value': '/customer/delete', 'checked': false, 'disabled': false}],
          "pages": [{'title': 'Page Add', 'value': '/page/add', 'checked': false, 'disabled': false},{'title': 'Page Edit', 'value': '/page/edit', 'checked': false, 'disabled': false},{'title': 'Page Delete', 'value': '/page/delete', 'checked': false, 'disabled': false}],
          "settingss": [{'title': 'Settings', 'value': '/settings/', 'checked': false, 'disabled': false}],
          "faqss": [{'title': 'FAQs', 'value': '/faqs/', 'checked': false, 'disabled': false}],
          "templates": [{'title': 'Email Template Add', 'value': '/template/add', 'checked': false, 'disabled': false},{'title': 'Email Template Edit', 'value': '/template/edit', 'checked': false, 'disabled': false},{'title': 'Email Template Delete', 'value': '/template/delete', 'checked': false, 'disabled': false}]
        };

    }

    PrivilegeChange() {
      this.roleForm.get('privilege').valueChanges.subscribe((Privilege: any) => {
        if(Privilege=="custom") {
          this.showDiv = true;
        } else {
          this.showDiv = false;        
        }
      });
    }

    onChange(event) {
      const roles = <FormArray>this.roleForm.get('roles') as FormArray;

      if(event.checked) {
        roles.push(new FormControl(event.source.value))
      } else {
        const i = roles.controls.findIndex(x => x.value === event.source.value);
        roles.removeAt(i);
      }
    }

    // convenience getter for easy access to form fields
    get f() { return this.roleForm.controls; }

    getRole(id) {
        this.roleService.getById(id).subscribe((data: any) => {
            var role = data.result_set;
            this.roleForm.setValue({
              roles_title: role.roles_title,
              privilege: role.privilege,
              roles: []
            });
            if(role.privilege=='custom') {
              this.showDiv = true;
              const roles = <FormArray>this.roleForm.get('roles') as FormArray;
              role.rules.forEach(permission=> {
                roles.push(new FormControl(permission));
                let variable = permission.split('/')[1].split('/')[0]+'s';   

                this.permissions[variable].forEach((item, index) => {
                  if(item.value==permission) {
                    this.permissions[variable][index].checked = true;
                  }
                });

             })
            }
        });
    }

    onSubmit() {
        this.submitted = true;

        // stop here if form is invalid
        if (this.roleForm.invalid) {
            return;
        }

        this.loading = true;

        const formData = new FormData();
        formData.append('roles_title', this.roleForm.get('roles_title').value);
        formData.append('privilege', this.roleForm.get('privilege').value);
        formData.append('rules', JSON.stringify(this.roleForm.get('roles').value));
        
        this.roleService.update(this.role_id, formData).pipe(first()).subscribe((data: any) => {
              if(data.status=='ok') {
                this.alertService.success(data.message, true);
                this.router.navigate(['/role']);
              } else {
                this.alertService.error(data.message, true);
              }
            },
            error => {
                this.alertService.error(error);
                this.loading = false;
            }
        );
    }
}