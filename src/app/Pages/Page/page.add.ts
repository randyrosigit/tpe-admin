import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { first } from 'rxjs/operators';
import {AngularEditorConfig} from '@kolkov/angular-editor';

import { AlertService, PageService } from '../../_services';
import { Utils } from '../../_helpers';

@Component({templateUrl: 'page.add.html'})
export class PageAdd implements OnInit {
    heading = 'Add Page';
    subheading = '';
    icon = 'fa fa-clone icon-gradient bg-happy-itmeo';
    pageForm: FormGroup;
    loading = false;
    submitted = false;

    wyswigconfig: AngularEditorConfig = {
      editable: true,
      spellcheck: true,
      height: '15rem',
      minHeight: '5rem',
      placeholder: 'Enter text here...',
      translate: 'no',
      customClasses: [
        {
          name: 'quote',
          class: 'quote',
        },
        {
          name: 'redText',
          class: 'redText'
        },
        {
          name: 'titleText',
          class: 'titleText',
          tag: 'h1',
        },
      ]
    };

    constructor(
        private formBuilder: FormBuilder,
        private router: Router,
        private pageService: PageService,
        private alertService: AlertService
    ) { }

    ngOnInit() {
        this.pageForm = this.formBuilder.group({
            page_name: ['', [Validators.required, Utils.noWhitespaceValidator]],
            page_router: ['', [Validators.required, Utils.noWhitespaceValidator]],
            page_description: ['', Validators.required],
            page_sort_order: ['', [Validators.required, Utils.noWhitespaceValidator]],
            page_status: ['', [Validators.required, Utils.noWhitespaceValidator]]
        });
    }

    // convenience getter for easy access to form fields
    get f() { return this.pageForm.controls; }

    onSubmit() {
        this.submitted = true;

        // stop here if form is invalid
        if (this.pageForm.invalid) {
            return;
        }

        this.loading = true;

        const formData = new FormData();
        formData.append('page_name', this.pageForm.get('page_name').value);
        formData.append('page_router', this.pageForm.get('page_router').value);
        formData.append('page_description', this.pageForm.get('page_description').value);
        formData.append('page_sort_order', this.pageForm.get('page_sort_order').value);
        formData.append('page_status', this.pageForm.get('page_status').value);

        this.pageService.insert(formData).pipe(first()).subscribe((data: any) => {
              if(data.status=='ok') {
                this.alertService.success(data.message, true);
                this.router.navigate(['/page']);
              } else {
                this.alertService.error(data.message, true);
              }
            },
            error => {
                this.alertService.error(error);
                this.loading = false;
            }
        );
    }
}