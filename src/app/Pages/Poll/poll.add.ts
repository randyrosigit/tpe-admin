import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { first } from 'rxjs/operators';

import { AlertService, MerchantService, PollService } from '../../_services';
import { Utils } from '../../_helpers';

@Component({ templateUrl: 'poll.add.html'})
export class PollAdd implements OnInit {
    heading = 'Add Poll';
    subheading = '';
    icon = 'fa fa-archive icon-gradient bg-happy-itmeo';
    pollForm: FormGroup;
    loading = false;
    submitted = false;
    poll_featured_poll = null;
    merchants = [];
    preview_featured_poll = null;


    constructor(
        private formBuilder: FormBuilder,
        private router: Router,
        private alertService: AlertService,
        private merchantServices: MerchantService,
        private pollService: PollService
    ) { }

    ngOnInit() {
        this.pollForm = this.formBuilder.group({
            poll_name: ['', [Validators.required, Utils.noWhitespaceValidator]],
            poll_description: ['', [Validators.required, Utils.noWhitespaceValidator]],
            poll_question: ['', [Validators.required, Utils.noWhitespaceValidator]],
            poll_featured_poll: ['', [Validators.required, Utils.noWhitespaceValidator]],
            poll_merchant_id: ['', [Validators.required, Utils.noWhitespaceValidator]],
            poll_option1: ['', [Validators.required, Utils.noWhitespaceValidator]],
            poll_option2: ['', [Validators.required, Utils.noWhitespaceValidator]],
            poll_option3: ['', Utils.noWhitespaceValidator],
            poll_option4: ['', Utils.noWhitespaceValidator]
            // poll_sort_order: ['']
        });
        this.loadAllMerchants();
    }

    private loadAllMerchants() {
        this.merchantServices.getAll({ params: {filter_merchant_status:1} }).pipe(first()).subscribe((merchants: any) => {
            if(merchants.status=='ok') {
                this.merchants = merchants.result_set;
            }
        });
    }

    IconChange(files: FileList) {
        this.poll_featured_poll = files[0];
        this.pollForm.get('poll_featured_poll').setValue(this.poll_featured_poll.name);
        var reader = new FileReader();
        reader.readAsDataURL(this.poll_featured_poll); 
        reader.onload = (_event) => { 
          this.preview_featured_poll = reader.result; 
        }
    }

    // convenience getter for easy access to form fields
    get f() { return this.pollForm.controls; }

    onSubmit() {
        this.submitted = true;

        // stop here if form is invalid
        if (this.pollForm.invalid) {
            return;
        }

        this.loading = true;

        const formData = new FormData();
        formData.append('poll_name', this.pollForm.get('poll_name').value);
        formData.append('poll_description', this.pollForm.get('poll_description').value);
        formData.append('poll_merchant_id', this.pollForm.get('poll_merchant_id').value);
        formData.append('poll_question', this.pollForm.get('poll_question').value);
        // formData.append('poll_sort_order', this.pollForm.get('poll_sort_order').value);
        formData.append('poll_option1', this.pollForm.get('poll_option1').value);
        formData.append('poll_option2', this.pollForm.get('poll_option2').value);
        formData.append('poll_option3', this.pollForm.get('poll_option3').value);
        formData.append('poll_option4', this.pollForm.get('poll_option4').value);
        formData.append('poll_featured_file', this.poll_featured_poll, this.poll_featured_poll.name);

        this.pollService.insert(formData).pipe(first()).subscribe((data: any) => {
            if(data.status=='ok') {
              this.alertService.success(data.message, true);
              this.router.navigate(['/poll']);
            } else {
              this.alertService.error(data.message, true);
            }
          },
          error => {
              this.alertService.error(error);
              this.loading = false;
          }
        );
    }
}