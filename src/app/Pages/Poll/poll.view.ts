import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { first } from 'rxjs/operators';

import { AlertService, PollService } from '../../_services';
import { Utils } from '../../_helpers';

@Component({templateUrl: 'poll.view.html'})
export class PollView implements OnInit {
    heading = 'View Poll';
    subheading = '';
    icon = 'fa fa-archive icon-gradient bg-happy-itmeo';
    pollForm: FormGroup;
    preview_featured_poll = null;
    poll_id: number;
    label = [];
    val = [];

    constructor(
        private formBuilder: FormBuilder,
        private router: Router,
        private route: ActivatedRoute,
        private alertService: AlertService,
        private pollService: PollService        
    ) {  
        this.poll_id = this.route.snapshot.params['id'];
    }

    ngOnInit() {
        this.getPoll(this.poll_id);
        this.pollForm = this.formBuilder.group({
            poll_name: ['', [Validators.required, Utils.noWhitespaceValidator]],
            poll_description: ['', [Validators.required, Utils.noWhitespaceValidator]],
            poll_question: ['', [Validators.required, Utils.noWhitespaceValidator]],
            poll_merchant_name: ['', [Validators.required, Utils.noWhitespaceValidator]],
            poll_option1: ['', [Validators.required, Utils.noWhitespaceValidator]],
            poll_option2: ['', [Validators.required, Utils.noWhitespaceValidator]],
            poll_option3: ['', [Validators.required, Utils.noWhitespaceValidator]],
            poll_option4: ['', [Validators.required, Utils.noWhitespaceValidator]],
            poll_total_vote: ['']
            // poll_sort_order: ['']
        });
    }

    // convenience getter for easy access to form fields
    get f() { return this.pollForm.controls; }

    getPoll(id) {
        this.pollService.getById(id).subscribe((data: any) => {
            var poll = data.result_set;
            this.pollForm.setValue({
              poll_name: poll.poll_name,
              poll_description: poll.poll_description,
              poll_merchant_name: poll.merchant_name,
              poll_question: poll.poll_question,
              poll_option1: poll.poll_option1,
              poll_option2: poll.poll_option2,
              poll_option3: poll.poll_option3,
              poll_option4: poll.poll_option4,
              poll_total_vote: poll.poll_total_vote
              // poll_sort_order: poll.poll_sort_order
            });
            this.preview_featured_poll = poll.poll_featured_poll;

            this.label = poll.result_labels;
            this.val = poll.result_values;
        });
    }

    getControlLabel(type: string){
     return this.pollForm.controls[type].value;
    }

}