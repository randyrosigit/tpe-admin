import { Component, OnInit, OnDestroy, ViewChild } from '@angular/core';
import { Subscription } from 'rxjs';
import { first } from 'rxjs/operators';
import {MatTableDataSource, MatSort, MatPaginator} from '@angular/material';
import {SelectionModel} from '@angular/cdk/collections';

// import { Poll } from '../../_models';
import { AlertService, PollService } from '../../_services';

@Component({ templateUrl: 'poll.list.html' })
export class PollList implements OnInit, OnDestroy {
    
    heading = 'Polls';
	  subheading = '';
	  icon = 'fa fa-archive icon-gradient bg-happy-itmeo';
	  link = '/poll/add';
    polls: [];
    dataSource: MatTableDataSource<any[]>;
    search_key = "";
    pageSizeOptions = null;

    displayedColumns = ['s_no','poll_name', 'merchant_name', 'poll_is_featured', 'poll_created_on', 'actions'];
    

    @ViewChild(MatPaginator) paginator: MatPaginator;  
    @ViewChild(MatSort) sort: MatSort;
    
    setPageSizeOptions(setPageSizeOptionsInput: string) {
      this.pageSizeOptions = setPageSizeOptionsInput.split(',').map(str => +str);
    }

    constructor( 
        private pollService: PollService,
        private alertService: AlertService ) { }

    ngOnInit() {
        this.loadAllPolls();
    }

    ngOnDestroy() {
    }

    clearFilters(){
       this.dataSource.filter = '';
       this.search_key = '';
    }

    private loadAllPolls() {
        this.pollService.getAll().pipe(first()).subscribe((polls: any) => {
        	if(polls.status=='ok') {
                this.dataSource = new MatTableDataSource(polls.result_set);
                this.dataSource.paginator = this.paginator;  
                this.dataSource.sort = this.sort; 
        	}
        });
    }

    deletePoll(id: number) {
        this.alertService.delete().then(data=>{
            if(data) {
                this.pollService.delete(id).pipe(first()).subscribe((data: any) => {
                    if(data.status=='ok') {
                      this.alertService.success(data.message, true);
                      this.loadAllPolls()
                    } else {
                      this.alertService.error(data.message, true);
                    }
                });
            } 
        });
    }

    featuredPoll(id: number) {
        this.pollService.featured(id).pipe(first()).subscribe((data: any) => {
            if(data.status=='ok') {
              this.alertService.success(data.message, true);
              this.loadAllPolls()
            } else {
              this.alertService.error(data.message, true);
            }
        });
    }

    applyFilter(filterValue: string) {  
      this.dataSource.filter = filterValue.trim().toLowerCase();  
    
      if (this.dataSource.paginator) {  
        this.dataSource.paginator.firstPage();  
      }  
    }

}
