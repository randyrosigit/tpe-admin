import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { first } from 'rxjs/operators';

import { AlertService, MerchantService, PollService } from '../../_services';
import { Utils } from '../../_helpers';

@Component({templateUrl: 'poll.edit.html'})
export class PollEdit implements OnInit {
    heading = 'Edit Poll';
    subheading = '';
    icon = 'fa fa-archive icon-gradient bg-happy-itmeo';
    pollForm: FormGroup;
    loading = false;
    submitted = false;
    poll_featured_poll = null;
    merchants = [];
    preview_featured_poll = null;
    poll_id: number;

    constructor(
        private formBuilder: FormBuilder,
        private router: Router,
        private route: ActivatedRoute,
        private alertService: AlertService,
        private merchantServices: MerchantService,
        private pollService: PollService        
    ) {  
        this.poll_id = this.route.snapshot.params['id'];
    }

    ngOnInit() {
        this.getPoll(this.poll_id);
        this.pollForm = this.formBuilder.group({
            poll_name: ['', [Validators.required, Utils.noWhitespaceValidator]],
            poll_description: ['', [Validators.required, Utils.noWhitespaceValidator]],
            poll_question: ['', [Validators.required, Utils.noWhitespaceValidator]],
            poll_featured_poll: ['', [Validators.required, Utils.noWhitespaceValidator]],
            poll_merchant_id: ['', [Validators.required, Utils.noWhitespaceValidator]],
            poll_option1: ['', [Validators.required, Utils.noWhitespaceValidator]],
            poll_option2: ['', [Validators.required, Utils.noWhitespaceValidator]],
            poll_option3: ['', Utils.noWhitespaceValidator],
            poll_option4: ['', Utils.noWhitespaceValidator]
            // poll_sort_order: ['']
        });
        this.loadAllMerchants();
    }

    // convenience getter for easy access to form fields
    get f() { return this.pollForm.controls; }

    getPoll(id) {
        this.pollService.getById(id).subscribe((data: any) => {
            var poll = data.result_set;
            this.pollForm.setValue({
              poll_name: poll.poll_name,
              poll_description: poll.poll_description,
              poll_featured_poll: poll.poll_featured_poll,
              poll_merchant_id: poll.poll_merchant_id,
              poll_question: poll.poll_question,
              poll_option1: poll.poll_option1,
              poll_option2: poll.poll_option2,
              poll_option3: poll.poll_option3,
              poll_option4: poll.poll_option4
              // poll_sort_order: poll.poll_sort_order
            });
            this.preview_featured_poll = poll.poll_featured_poll;
        });
    }

    private loadAllMerchants() {
        this.merchantServices.getAll().pipe(first()).subscribe((merchants: any) => {
            if(merchants.status=='ok') {
                this.merchants = merchants.result_set;
            }
        });
    }

    IconChange(files: FileList) {
        this.poll_featured_poll = files[0];
        this.pollForm.get('poll_featured_poll').setValue(this.poll_featured_poll.name);
        var reader = new FileReader();
        reader.readAsDataURL(this.poll_featured_poll); 
        reader.onload = (_event) => { 
          this.preview_featured_poll = reader.result; 
        }
    }

    onSubmit() {
        this.submitted = true;

        // stop here if form is invalid
        if (this.pollForm.invalid) {
            return;
        }

        this.loading = true;

        const formData = new FormData();
        formData.append('poll_name', this.pollForm.get('poll_name').value);
        formData.append('poll_description', this.pollForm.get('poll_description').value);
        formData.append('poll_merchant_id', this.pollForm.get('poll_merchant_id').value);
        // formData.append('poll_sort_order', this.pollForm.get('poll_sort_order').value);
        formData.append('poll_question', this.pollForm.get('poll_question').value);
        formData.append('poll_option1', this.pollForm.get('poll_option1').value);
        formData.append('poll_option2', this.pollForm.get('poll_option2').value);
        formData.append('poll_option3', this.pollForm.get('poll_option3').value);
        formData.append('poll_option4', this.pollForm.get('poll_option4').value);

        if(this.poll_featured_poll!=null) {
          formData.append('poll_featured_file', this.poll_featured_poll, this.poll_featured_poll.name);
        }

        this.pollService.update(this.poll_id, formData).pipe(first()).subscribe((data: any) => {
              if(data.status=='ok') {
                this.alertService.success(data.message, true);
                this.router.navigate(['/poll']);
              } else {
                this.alertService.error(data.message, true);
              }
            },
            error => {
                this.alertService.error(error);
                this.loading = false;
            }
        );
    }
}