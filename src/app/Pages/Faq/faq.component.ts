import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { Validators, FormBuilder, FormGroup, FormControl, FormArray } from '@angular/forms';
import { first } from 'rxjs/operators';

import { AlertService, PageService } from '../../_services';

@Component({
  selector: 'my-faqs',
  templateUrl: './faq.component.html'
})
export class FAQComponent  {

  heading = 'FAQs';
  subheading = '';
  icon = 'fa fa-question icon-gradient bg-happy-itmeo';
  exampleForm: FormGroup;
  loading = false;

  constructor( private formBuilder: FormBuilder, private pageservice: PageService, private alertService: AlertService ) { }

  ngOnInit () {
    this.exampleForm = this.formBuilder.group({
      units: this.formBuilder.array([
         // this.getUnit()
      ])
    });
    this.edit_data();
  }

  ngOnDestroy(): void {}

  private getUnit(question='', answer='') {
    return this.formBuilder.group({
      question: [question, Validators.required],
      answer: [answer, Validators.required]
    });
  }

  private edit_data() {
    this.pageservice.getFaqs().pipe(first()).subscribe((data: any) => {
      if(data.status=='ok') {
        const faqs = data.result_set;        

        for(let faq of faqs){
          const control = <FormArray>this.exampleForm.controls['units'];
          control.push(this.getUnit(faq.question, faq.answer));
        }
      }
    });
  }

  private addUnit() {
    const control = <FormArray>this.exampleForm.controls['units'];
    control.push(this.getUnit());
  }

  private removeUnit(i: number) {
    const control = <FormArray>this.exampleForm.controls['units'];
    control.removeAt(i);
  }

  save(model: any) {
    if (this.exampleForm.invalid) {
        return;
    }

    const formData = new FormData();
    formData.append('questions', JSON.stringify(this.exampleForm.get('units').value));

    this.pageservice.faqinsert(formData).pipe(first()).subscribe((data: any) => {
      if(data.status=='ok') {
        this.alertService.success(data.message, true);
      } else {
        this.alertService.error(data.message, true);
      }
    },
    error => {
      this.alertService.error(error);
      this.loading = false;
    });

  }

}
